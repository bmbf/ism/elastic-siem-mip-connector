# SIEM-MIP-Connector

- [Deutsch](#german)
- [English](#english)

## German

Ein Powershellskript zur automatisierten Datenübermittlung an das Melde- und Informationsportal des BSI
Dieses Powershellskript ermöglicht die automatisierte Extraktion von sicherheitsrelevanten Ereignissen aus einem SIEM (Security Information and Event Management) auf Basis eines Elastic Stacks und die Übermittlung dieser Ereignisse über die API des Melde- und Informationsportals des BSI.
Mithilfe dieses Skripts erfolgen im BMBF die statistischen Monatsmeldungen nach der Allgemeine Verwaltungsvorschrift über das Meldeverfahren gemäß § 4 Abs. 6 BSIG vollständig automatisiert. Dadurch entlasten wir unser ISM-Team und haben freie Kapazitäten für die wirklich sicherheitsrelevanten Dinge.

### Anforderungen

- PowerShell 5.0 oder höher
- Zugriff auf ein SIEM mit einem Elastic Stack
- Zugangsdaten für das SIEM und das BSI-Meldeportal

### Installation und Verwendung

1. **Konfiguration:** Bearbeiten Sie die `config.json`-Datei und ersetzen Sie die Platzhalter durch Ihre Zugangsdaten für das SIEM und das BSI-Meldeportal sowie die sonstigen entsprechenden Werte.
2. **Ausführung des Skripts:** Führen Sie das Skript `siem-mip-connector.ps1` aus, um sicherheitsrelevante Ereignisse aus dem SIEM zu extrahieren und automatisch an das BSI-Meldeportal zu übermitteln.
3. **Automatisierung:** Richten Sie einen Cronjob in der Aufgabenplanung unter Windows ein, die das Skript jeweils zum Monatsletzten ausführt.

### Konfigurationsdatei

Die `config.json`-Datei enthält die Konfiguration für das Skript, einschließlich der Zugangsdaten für das SIEM und das BSI-Meldeportal sowie der spezifischen Filter für die Extraktion von Ereignissen aus dem SIEM.

Deatils zu den möglichen Werten entnehmen Sie bitte der API-Dokumentation, die als PDF über das BSI-Meldeportal erhältlich ist.

#### Sicherheitshinweis

Beschränken Sie die Zugriffsrechte auf die `config.json` soweit wie möglich! Zugriff auf die Datei sollte nur der Service-Account haben, mit dem das Skript gestartet wird.

### Parameter

- **Lokation der Konfigurationsdatei:** Über den Parameter `-configFilePath` kann dem Skript die Lokation der Konfigurationsdatei mitgeteilt werden.
- **Lokation der Logdatei:** Über den Parameter `-logFilePath` kann dem Skript die Lokation der Logdatei mitgeteilt werden.

Durch die Übergabe der Parameter entfällt die Nachfrage, die manuell ausgefüllt werden muss. Dies ist besonders bei der automatischen Ausführung über einen Cronjob praktisch.

### Funktionen

- Automatisierte Extraktion von Ereignissen aus einem Elastic Stack-basierten SIEM
- Automatisierte Generierung eines Bearer-Tokens für die API des BSI-Meldeportals
- Angemessene Fehlerbehandlung
- Protokollierung von Ereignissen in einer Logdatei
- Komfortable Konfiguration über die `config.json`-Datei

### Roadmap / Backlog

Folgende Erweiterungen und Use Cases haben wir noch im Backlog:

- Weiterentwicklung zur Verwendung mit SOFORT-Meldungen
- Verfeinerung der Prüfung der Einträge auf ihre maximale Länge
- Bessere Verwahrung der Credentials

### Lizenz

Dieses Skript steht unter der WTFPL-Lizenz.

Hinweis: Bitte beachten Sie, dass dieses Skript für den Einsatz in einer spezifischen Umgebung entwickelt wurde und möglicherweise an Ihre Anforderungen angepasst werden muss.

## English

A PowerShell script for automated data submission to the Federal Office for Information Security's Reporting and Information Portal (MIP)
This PowerShell script enables the automated extraction of security-related events from a SIEM (Security Information and Event Management) based on an Elastic Stack and the submission of these events via the API of the Federal Office for Information Security's Reporting and Information Portal (MIP).
Using this script, the statistical monthly reports are generated fully automated according to the General Administrative Regulation on Reporting Procedures pursuant to § 4 (6) of the BSI Act in the BMBF. This relieves our ISM team and provides free capacities for truly security-relevant matters.

### Requirements

- PowerShell 5.0 or higher
- Access to a SIEM with an Elastic Stack
- Credentials for the SIEM and the BSI Reporting Portal

### Installation and Usage

1. **Configuration:** Edit the `config.json` file and replace the placeholders with your credentials for the SIEM and the BSI Reporting Portal, as well as the appropriate values.
2. **Executing the script:** Run the `siem-mip-connector.ps1` script to extract security-related events from the SIEM and automatically submit them to the BSI Reporting Portal.
3. **Automation:** Set up a cron job in the Task Scheduler on Windows to run the script on the last day of each month.

### Configuration File

The `config.json` file contains the configuration for the script, including credentials for the SIEM and the BSI Reporting Portal, as well as specific filters for extracting events from the SIEM.

Please refer to the API documentation, available as a PDF via the BSI reporting portal, for details on the possible values.

#### Security Notice

> **Security Notice:**
>
> Limit access rights to the `config.json` file as much as possible! Access to the file should only be granted to the service account used to start the script.

### Parameters

- **Location of the configuration file:** The `-configFilePath` parameter allows you to specify the location of the configuration file to the script.
- **Location of the log file:** The `-logFilePath` parameter allows you to specify the location of the log file to the script.

Passing these parameters eliminates the need for manual input, which is especially useful when running the script automatically via a cron job.

### Features

- Automated extraction of events from an Elastic Stack-based SIEM
- Automated generation of a Bearer token for the BSI Reporting Portal API
- Proper error handling
- Logging of events in a log file
- Convenient configuration via the `config.json` file

### Roadmap / Backlog

The following extensions and use cases are still in the backlog:

- Further development for use with IMMEDIATE reports
- Refinement of entry validation for maximum length
- Improved storage of credentials

### License

This script is licensed under the WTFPL license.

Note: Please be aware that this script was developed for use in a specific environment and may need to be adapted to your requirements.
