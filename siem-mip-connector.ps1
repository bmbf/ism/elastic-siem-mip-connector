# Übergabe der obligatorischen Datenlokationen für die Konfiguration und die Logdaten
param (
    [Parameter(Mandatory=$true)]
    [string]$ConfigFilePath,

    [Parameter(Mandatory=$true)]
    [string]$LogFilePath
)

# Überprüfen, ob die Konfigurationsdatei am angegebenen Ort existiert
if (-not (Test-Path -Path $ConfigFilePath -PathType Leaf)) {
    Write-Host "Die Konfigurationsdatei '$ConfigFilePath' existiert nicht an dem angegebenen Pfad." -ForegroundColor Red
    exit
}

# Überprüfen, ob das Verzeichnis für die Logdatei existiert
$LogDirectory = Split-Path $LogFilePath
if (-not (Test-Path -Path $LogDirectory -PathType Container)) {
    Write-Host "Das Verzeichnis '$LogDirectory' für die Logdatei existiert nicht." -ForegroundColor Red
    exit
}

# Funktion zur Protokollierung von Nachrichten in eine Logdatei
function Write-Log {
    param(
        [string]$Message,
        [string]$LogPath
    )
    
    # Datum und Uhrzeit für die Protokollierung
    $TimeStamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
    # Nachricht zusammenstellen
    $LogMessage = "$TimeStamp - $Message"
    
    # Protokolldatei öffnen und Nachricht hinzufügen
    Add-content -Path $LogPath -Value $LogMessage
}

# Sicherstellen, dass eine mit dem SIEM/SOC kompatible TLS-Version in der PowerShell-Umgebung aktiv ist
try {
    # Überprüfen, ob TLSv1.2 aktiviert ist
    if (-not ([Net.ServicePointManager]::SecurityProtocol -band [Net.SecurityProtocolType]::Tls12)) {
        # TLSv1.2 aktivieren
        [Net.ServicePointManager]::SecurityProtocol += [Net.SecurityProtocolType]::Tls12
        $ErrorMessage = "INFO - TLSv1.2 wurde aktiviert."
        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
    } else {
        $ErrorMessage = "INFO - TLSv1.2 ist bereits aktiviert."
        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
    }

    # Überprüfen, ob TLSv1.3 aktiviert ist
    if (-not ([Net.ServicePointManager]::SecurityProtocol -band [Net.SecurityProtocolType]::Tls13)) {
        # TLSv1.3 aktivieren
        [Net.ServicePointManager]::SecurityProtocol += [Net.SecurityProtocolType]::Tls13
        $ErrorMessage = "INFO - TLSv1.3 wurde aktiviert."
        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
    } else {
        $ErrorMessage = "INFO - TLSv1.3 ist bereits aktiviert."
        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
    }
} catch {
    $ErrorMessage = "ERROR - Fehler beim Aktivieren einer kompatiblen TLS-Version: $_"
    Write-Log -Message $ErrorMessage -LogPath $LogFilePath

}

# Funktion zur Überprüfung der maximalen Zeichenlänge aller Werte für Felder, die auf "_details" enden
function ValidateMaxStringLength {
    param(
        [string]$Value
    )

    if ($Value.Length -gt 1000) {
        return $false
    } else {
        return $true
    }
}

# Einlesen der Konfigurationsdatei
try {
  # Inhalt einlesen
  $configInhalt = Get-Content -Raw $ConfigFilePath | ConvertFrom-Json

  # Umwandeln des JSON-Objekts in ein PSObject
  $configInhalt_psObject = [PSCustomObject]$configInhalt
 
  # Erzeugen einer Memory-Tabelle und Befüllen mit Vorab-Informationen aus der Konfigurationsdatei
  $MemoryTabelle = @{}
    function BefuelleMemoryTabelle {
        param (
            [ref]$Tabelle,  # Verweis auf die Hash-Tabelle
            [string]$schluessel,  # Schlüsselwert = Filtername
            [string]$wert  # Wert = Filterinhalt
        )

        # Überprüfen der maximalen Zeichenlänge des Werts
        if (ValidateMaxStringLength -Value $wert) {
        # Hinzufügen des Schlüssel-Wert-Paars zur Hash-Tabelle
        $Tabelle.Value[$schluessel] = $wert
        } else {
            $ErrorMessage = "ERROR - Der Wert für '$schluessel' überschreitet die maximal zulässige Zeichenlänge von 1000 Zeichen."
            Write-Log -Message $ErrorMessage -LogPath $LogFilePath
            exit
          }

    }

    if ($configInhalt_psObject.BSI.values) {
        $properties = $configInhalt_psObject.BSI.values | Get-Member -MemberType NoteProperty
        $propertyCount = $properties.Count

        # Iterieren durch die Eigenschaften der Memory-Tabelle
        foreach ($property in $properties) {
            try {

                # Anpassen des Berichtszeitraums an den aktuellen Berichtsmonat bei Ausführung
                if ($property.Name -eq "berichtszeitraum") {
                    $value = Get-Date -Format "yyyy-MM"
                }
                # Abrufen des Werts der aktuellen Eigenschaft
                else {
                    $value = $configInhalt_psObject.BSI.values.$($property.Name)
                }

                # Verwenden der Inhalte der aktuellen Eigenschaft
                BefuelleMemoryTabelle -Tabelle ([ref]$MemoryTabelle) -schluessel $property.Name -wert $value
            } catch {
                $ErrorMessage = "ERROR - Fehler beim Befüllen der Memory-Tabelle: $_"
                Write-Log -Message $ErrorMessage -LogPath $LogFilePath
            }
        }
    }

    # Erstellen der Funktion GetSIEMData für den Abruf der einzelnen Werte aus dem SIEM/SOC
    function GetSIEMData {
     
        # Übergeben der Variablenn
        param ( 
            [string]$SIEMURL
        )

        try {

            # Auslesen der API-Zugangsdaten aus der Konfigurationsdatei
            $SIEMUser = $configInhalt_psObject.SIEM.user
            $SIEMPassword = $configInhalt_psObject.SIEM.password

            if ($configInhalt_psObject.SIEM.filter) {
                $properties = $configInhalt_psObject.SIEM.filter | Get-Member -MemberType NoteProperty
                foreach ($property in $properties) {
                    try {
                        $value = $configInhalt_psObject.SIEM.filter.$($property.Name)
                        $elasticsearchUrl = $SIEMURL + $value

                        # Erstellen eines Basisauthentifizierungs-Strings
                        $authInfo = ($SIEMUser + ":" + $SIEMPassword)
                        $authInfoEncoded = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes($authInfo))
                        $Headers = @{
                            "Authorization" = "Basic $authInfoEncoded"
                        }

                        # Aufrufen der Elasticsearch Count-API und Speichern des zurückgegebenen Werts in der Variable (JSON-Format)
                        $Ergebnis_SIEM_JSON = Invoke-RestMethod -Uri $elasticsearchUrl -Method GET -Headers $Headers
                        $Ergebnis_SIEM_PSO = [PSCustomObject]$Ergebnis_SIEM_JSON
                        BefuelleMemoryTabelle -Tabelle ([ref]$MemoryTabelle) -schluessel $property.Name -wert $Ergebnis_SIEM_PSO.count
                    } catch {
                        $ErrorMessage = "ERROR - Fehler beim Zugriff auf die Elasticsearch Count-API: $_"
                        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
                    }
                }
            }
        } catch { 
            $ErrorMessage = "ERROR - Fehler beim Zugriff auf die Elasticsearch Count-API: $_"
            Write-Log -Message $ErrorMessage -LogPath $LogFilePath
        }
    }

    # Aufrufen der Funktion GetSIEMData
    GetSIEMData -SIEMURL $configInhalt_psObject.SIEM.url

    # Konvertieren der Memory-Tabelle in das JSON-Format
    $jsonfromTabelle = $MemoryTabelle | ConvertTo-Json

    # Ausgeben der befüllten Memory-Tabelle mit den Werten der Elasticsearch Count-API auf der Konsole zu Debugzwecken
    $jsonfromTabelle

    # Generieren des Bearer-Tokens bei der MIP-API des BSI
    # Einlesen der MIP-API-Credentials aus der Konfigurationsdatei
    $BSIUser = $configInhalt_psObject.BSI.user
    $BSIPassword = $configInhalt_psObject.BSI.password

    # Vorbereiten des Body-Tokens für die Authentifizierung an der MIP-API
    $body_token = @{
        username = $BSIUser
        password = $BSIPassword
    } | ConvertTo-Json

    try {
        # Abrufen des Bearer-Tokens von der MIP-API
        $BearerToken = Invoke-RestMethod -Uri $configInhalt_psObject.BSI.'authentification-url' -Method Post -Headers @{
            "accept" = "application/json"
            "Content-Type" = "application/json"
        } -Body $body_token

        # Überprüfen, ob der Bearer-Token erfolgreich abgerufen wurde
        if ($BearerToken.access) {

            # Formatieren des Bearer-Tokens im Autorisierungsheader
            $Headers = @{
                "X-ACCESS-TOKEN" = "Bearer $($BearerToken.access)"
                "Content-Type" = "application/json"
            }

            # Vorbereiten der meldepflichtigen Daten zur Übermittlung an die MIP-API
            $submitData = @{
                values = $jsonfromTabelle
                type_of_report = $configInhalt_psObject.BSI.type_of_report
                previous_submission_id = $configInhalt_psObject.BSI.previous_submission_id
                first_submission_id = $configInhalt_psObject.BSI.first_submission_id
                form_id = $configInhalt_psObject.BSI.form_id
                institution = $configInhalt_psObject.BSI.institution
            } | ConvertTo-Json

            # Senden der meldepflichtigen Daten an die MIP-API
            $submit = Invoke-RestMethod -Uri $configInhalt_psObject.BSI.'submit-url' -Method Post -Headers $Headers -Body $submitData

            # Überprüfen, ob die meldepflichtigen Daten an die MIP-API erfolgreich übermittelt wurden
            if ($submit.status -eq "success" -and $submit.code -eq 200 -and $submit.message -eq "Der Report wurde erfolgreich abgeschickt.") {
                $ErrorMessage = "INFO - Erfolgreiche Übermittlung der meldepflichtigen Daten."
                Write-Log -Message $ErrorMessage -LogPath $LogFilePath
            } else {
                $ErrorMessage = "ERROR - Fehler bei der Übermittlung der meldepflichtigen Daten an die MIP-API: $($submit.message)"
                Write-Log -Message $ErrorMessage -LogPath $LogFilePath
            }
        } else {
            $ErrorMessage = "ERROR - Fehler beim Abrufen des Bearer-Tokens bei der MIP-API: $_"
            Write-Log -Message $ErrorMessage -LogPath $LogFilePath
        }
    } catch {
        $ErrorMessage = "ERROR - Fehler beim Abrufen des Bearer-Tokens bei der MIP-API: $_"
        Write-Log -Message $ErrorMessage -LogPath $LogFilePath
    }
} catch {
    $ErrorMessage = "ERROR: Fehler beim Einlesen der Konfigurationsdatei: $_"
    Write-Log -Message $ErrorMessage -LogPath $LogFilePath
}
